+++

date = "2016-12-05T14:41:00+01:00"
title = "Services"
author = "Framasoft"
draft = false
type = "page"
id = "-services"
[menu]
     [menu.main]
        name = "Services"
        weight = 1
        identifier = "services"
+++

</p>
{{% grid class="row" %}}
{{% grid class="col-sm-8 col-sm-offset-4" %}}

# Services
## Créer et proposer des outils

Bienvenue dans un monde où les outils sont conçus de manière à favoriser des échanges apaisés&nbsp;!

Entre 2017 et 2018, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> veut participer à l’élaboration et à la mise à disposition de nouveaux services Web, conçus afin de respecter les personnes, leurs libertés, leurs échanges et leurs réseaux.

Ces services seront développés en collaboration avec les personnes qui les utiliseront pour s’enrichir de ces échanges.

{{% /grid %}}
{{% /grid %}}
<!-- service framasite -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/services-framasite.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### Framasite, ouvrir des espaces web

Créer une page web, un site, un blog, ou même un wiki&hellip; trop souvent, ces moyens d’expression et de partage restent complexes à apprivoiser, d’autant plus si on ne veut pas livrer son intimité à des plateformes privées.

<a href="https://frama.site"><b class="violet">Frama</b><b class="vert">site</b></a> vous propose les instruments en ligne pour vous faire votre place sur la toile. Ce service vous ouvre un lieu d’expression dont les contenus vous appartiennent, dans un espace d’hébergement qui respecte les données (et donc les vies) des personnes qui viendront visiter votre site web et y apporter leur contribution&nbsp;?

<a href="https://frama.site"><b class="violet">Frama</b><b class="vert">site</b></a> ne sera pas parfait dès le lancement. Grâce à vos retours, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> pourra apprendre à en simplifier l’usage et contribuer au développement de ces logiciels qui facilitent la création de sites web éthiques&hellip; pour tou·te·s.

{{% /grid %}}
{{% /grid %}}
<!-- service framameet -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/services-framameet2.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### Framameet, favoriser réunions et rencontres

Échanger en ligne, c’est parfois une occasion de se rencontrer de visu, loin des claviers. Lorsque l’on organise ces regroupements avec les outils _Facebook_ ou via des plateformes à la _MeetUp_, on abandonne à ces ogres dévoreurs de données toutes les informations des membres du groupe.

Les communautés du logiciel libre s’intéressent à la création d’outils de ce type, dans le respect des libertés fondamentales des personnes désireuses de se rassembler.

<a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> souhaite accompagner un tel projet et contribuer à son développement&hellip; jusqu’à aboutir à un service libre, ouvert à tou·te·s, et au service de notre vivre ensemble.


{{% /grid %}}
{{% /grid %}}
<!-- service framapetitions -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/services-framapetitions2.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}
    
### Framapetitions, faire entendre les opinions

Les pétitions en ligne constituent un outil à la fois stratégique et sensible. Nos rassemblements d’opinions sur le monde ne devraient pas être verrouillés par des logiciels obscurs ni exploités par des entreprises privées.

Lorsque <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> a créé le service de formulaires en ligne <a href="https://framaforms.org"><b class="violet">Frama</b><b class="vert">forms</b></a>, il était clair qu’une adaptation du code de ce service pourrait déboucher sur Framapetitions. Il fallait néanmoins attendre de voir comment cette création pourrait supporter une utilisation massive.

Forte de plus d’un an d’expérience à héberger <a href="https://framaforms.org"><b class="violet">Frama</b><b class="vert">forms</b></a>, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> veut créer puis mettre en ligne Framapetitions, qui nécessitera vos contributions sous forme de tests, retours d’expérience et soutiens.


{{% /grid %}}
{{% /grid %}}
<!-- service framapetube -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/services-framatube.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### Framatube, brisons l'hégémonie de YouTube

L’hébergement de vidéos, lorsqu’on le centralise sur une plateforme, pose un problème technique et économique important. Si vos vidéos sont nombreuses cela vous coûtera cher en espace d’hébergement. Si elles ont du succès, cela générera un trafic important (un embouteillage depuis votre serveur&nbsp;!) et pourra vous être facturé.

Plutôt que d’utiliser la méthode centralisatrice, qui permet à _YouTube_, _Netflix_ (et d’autres&hellip;) d’être devenus incontournables, autant s’inspirer des méthodes qui ont permis de créer Internet&nbsp;: la décentralisation et le partage de pair-à-pair&nbsp;! 

Le logiciel libre _PeerTube_ permet la création d’une fédération d’hébergeurs de vidéos qui seront diffusées en pair-à-pair.  Ainsi, chaque ordinateur recevant une vidéo l’envoie en même temps aux autres, et chaque hébergeur peut décider de ses propres règles du jeu (conditions d’utilisation, modération, monétisation&hellip;) tout en étant relié à d'autres hébergeurs de la fédération.

<a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> fait le pari de financer le développeur de _PeerTube_, non pas pour héberger les vidéos de chacun·e sur un Framatube, mais dans la volonté que des artistes, associations, organisations, institutions et médias puissent héberger leurs propres plateformes indépendantes de vidéos et contribuer à leur tour à ce projet.


{{% /grid %}}
{{% /grid %}}
<p>
