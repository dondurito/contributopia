+++

date = "2016-12-05T14:41:00+01:00"
title = "Hiving off"
author = "Framasoft"
draft = false
type = "page"
id = "-essaimage"
[menu]
     [menu.main]
        name = "Hiving off"
        weight = 1
        identifier = "essaimage"
+++

</p>
{{% grid class="row" %}}
{{% grid class="col-sm-8 col-sm-offset-4" %}}

# Hiving off
## Propagating know-how

Welcome to a world where the ethical cloud is in reach of each and every one of us!

For 2018 and 2019 <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> will prioritise actions which encourage digital autonomy, in order to offer to as many as possible a solidarity hosting of our digital life that is trustworthy.

A process of hiving off which has already started and which needs refining in order to offer real local alternatives to the enterprise-silos which harvest our data.

{{% /grid %}}
{{% /grid %}}
<!-- chatons -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/essaimage-chatons.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### <abbr title="Collective of Keen Internet Talented Teams Engaged in Network Services">KITTENS</abbr> for local, ethical, solidarity hosting

Inaugurated in October 2016 on the initiative of <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a>, the Collective of Keen Internet Talented Teams Engaged in Network Services is both a self-managed label and a space for mutual aid.

For the public, the <abbr title="Collective of Keen Internet Talented Teams Engaged in Network Services">KITTENS</abbr> label indicates a local host in whom you can trust your data with confidence. This confidence is based on a number of strictly followed commitments that come from values like openness, transparency, neutrality and respecting personal data.

For the members of the collective, <abbr title="Collective of Keen Internet Talented Teams Engaged in Network Services">KITTENS</abbr> is a shared tool for mutual support through, amongst other things, the sharing of the legal, technical and interpersonal knowledge necessary to be worthy of the trust invested by the beneficiaries of the services.

<a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> will take the time to listen to and respond to the needs of the collective. It is the latter who will decide the course of action in order to grow. This is done in the hope that one day each and every one of us will be able to find a KITTEN in our neighbourhood!


{{% /grid %}}
{{% /grid %}}
<!-- yunohost -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/essaimage-yunohost.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### YUNOHOST, easy self hosting

YUNOHOST is a free software project which aims to give access to even the least tech savvy of us to the self hosting of services. Emails, file sharing, agendas, tools for collaborative creation and organisation: and all that within reach of a few clicks in order to have your own server (and the data) at home for you, your friends and family, your organisation, your business, your collective&hellip;

Since January 2017, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> has been investing its employees worktime in order to be sure that there can be a maximum of the libre services from the "<a href="https://degooglisons-internet.org"><b class="violet">De-google-ify</b>&nbsp;<b class="orange">Internet</b></a>" campaign available in the YUNOHOST solution.

It is in this spirit of collaboration with YUNOHOST that <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> is accompanying and promoting this solution in order that it will be adopted on a massive scale.


{{% /grid %}}
{{% /grid %}}
<!-- i18n -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/essaimage-i18n.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### Internationalisation: sharing our experience beyond our borders

With the "<a href="https://degooglisons-internet.org"><b class="violet">De-google-ify</b>&nbsp;<b class="orange">Internet</b></a>" campaign, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> is offering more than 30 ethical and alternative web services to a French speaking public. It is out of the question to translate the services hosted by <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a>: that would result in too many users and too much weight on the shoulders of a small French association!

Nevertheless, because the campaign offers a solution that is (more or less) complete, an experience (relatively) perfected and because it has resulted in a participation that is (modestly) remarkable, the <a href="https://degooglisons-internet.org"><b class="violet">De-google-ify</b>&nbsp;<b class="orange">Internet</b></a> project seems to be more or less unique in the world&hellip; and that could change!

There is a vast work of construction here for which the foundations need laying, that of sharing the years of _de-google-ification_ so that others can take inspiration and apply it in their language, adapting it to their culture.

In effect, it means just transforming, together, the history of "<a href="https://degooglisons-internet.org"><b class="violet">De-google-ify</b>&nbsp;<b class="orange">Internet</b></a>" into an international commons. Nothing more than that!

{{% /grid %}}
{{% /grid %}}
<!-- winter of code -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/essaimage-woc.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### Framasoft _Winter of code_: _winter is coding_

With the _Google Summer of Code_, the web giant is indeed crafty: financing the _open source_ projects of developers allows _Google_ to handpick the codes that will be of benefit, and at the same time to seduce talented coders, to format them into its business culture, all the while waving the _Google_ banner.

In addition, the world of digital freedoms is becoming more and more dependent on the financial contributions of _Google_ and the other web giants in order to survive and it is becoming worrying.

By proposing the _Winter of Code_, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> wishes to do its bit to invert this tendency. The heart of the idea is to put free software communities in contact with trainees in information technology seeking experience in an environment that makes sense, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> will bring administrative and perhaps even financial support.

All that remains to be imagined with the interested parties but one thing is sure: _Winter is Coding_!

{{% /grid %}}
{{% /grid %}}
<p>
