+++

date = "2016-12-05T14:41:00+01:00"
title = "Prima Pagina"
author = "Framasoft"
draft = false
type = "page"
id = "-home"
[menu]
     [menu.main]
        name = "Home"
        weight = 1
        identifier = "home"
+++

# Contributopia
## Degooglizzare non basta

<a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> ti invita a unirti in un'avventura comune: esplorare i mondi digitali dove l'umanità e le nostre libertà fondamentali sono rispettate, un rispetto che parte fin dalla progettazione degli strumenti che usiamo.

Durante questo viaggio, che durerà dal 2017 al 2020, cerchiamo insieme di concretizzare gli strumenti digitali per facilitare i contributi di tutti•e, in ogni campo di azione e immaginazione.

[maggiori informazioni](https://framablog.org/2017/10/09/contributopia-degooglizzare-non-basta/)

</p>
{{% grid class="row" %}}
{{% grid class="col-sm-4 text-center" %}}

[![](/menu-services.png)](../services)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### 2017-2018: Creare ed offrire strumenti

A <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a>, forti di tre anni passati a <a href="https://degooglisons-internet.org"><b class="violet">Degooglizzare</b>&nbsp;<b class="orange">Internet</b></a> – che ha messo in piedi più di trenta servizi alternativi –, sappiamo che i giganti del web non ci rubano solo le nostre vite digitali: in modo sistematico modellano le nostre abitudini, le nostre azioni, perfino il modo in cui pensiamo.

Nell'esplorazione di questo primo mondo, troverai servizi on-line progettati su una diversa scala di valori, che rispettano le vite (private) degli utenti e le diversità che fanno parte delle nostre reti di relazioni. Un mondo dove si concepiscono gli strumenti per condividere le nostre idee, le nostre opinioni, i nostri eventi... e perfino i nostri video!

[maggiori informazioni](../services)

{{% /grid %}}
{{% /grid %}}
{{% grid class="row" %}}
{{% grid class="col-sm-4 text-center" %}}

[![](/menu-essaimage.png)](../essaimage)

{{% /grid %}}
{{% grid class="col-sm-8" %}}
    
### 2018-2019: Trasmettere le competenze

I giganti del web si fondano sulla centralizzazione dei nostri dati, seducendo gli utenti con servizi che in realtà sequestrano le nostre esistenze digitali in giganteschi silos digitali pieni di nostri dati: tracciano i nostri click, le nostre conversazioni, le nostre vite private. Riprodurre questo modello centralistico significherebbe accentrare i poteri con tutti i pericoli che ne conseguono.

Andiamo a scoprire un mondo in cui non mettiamo tutte le nostre vite in un solo cesto digitale! Un mondo in cui ci organizziamo, contro i supermercati digitali che fanno affari sui nostri dati, in "cooperative" digitali di prossimità e mutuo soccorso. Un mondo dove si moltiplichino i "Gattini" (<abbr title="Collettivi di Hosting Alternativi Trasparenti Open Neutrali Solidali">CHATONS</abbr>), nuvole digitali personali, dove ci insegniamo a vicenda con cross-impollinazioni internazionali.

[maggiori informazioni](../essaimage)

{{% /grid %}}
{{% /grid %}}
{{% grid class="row" %}}
{{% grid class="col-sm-4 text-center" %}}

[![](/menu-educ-pop.png)](../educ-pop)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### 2019-2020: Ispirare le potenzialità

C'è una visione dietro i servizi che offrono i giganti del web. È quella che riduce la nostra intimità a merce per il vantaggio di inserzionisti di pubblicità commerciali. Quella che sminuisce il nostro ruolo a meri consumatori, che rinunciano alla propria libertà per pigrizia. Non c'è spazio nei loro piani per quegli strumetni digitali che favoriscono le nostre potenzialità, la partecipazione, l'educazione popolare.

Resta ancora un mondo ignoto da co-creare e dissodare insieme, equipaggiati con strumenti pensati per sviluppare nuovi modi di "lavorare in comune" e creare beni comuni. Un mondo ispirato a innumerevoli comunità (tra cui la comunità del software libero), per aiutare a scegliere gli strumenti a oguno•a più adatti, per l'autonomia digitale, per collaborare in modo diverso e condividere il sapere.

[maggiori informazioni](../educ-pop)

{{% /grid %}}
{{% /grid %}}
<p>
