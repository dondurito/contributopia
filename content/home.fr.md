+++

date = "2016-12-05T14:41:00+01:00"
title = "Accueil"
author = "Framasoft"
draft = false
type = "page"
id = "-home"
[menu]
     [menu.main]
        name = "Accueil"
        weight = 1
        identifier = "home"
+++

# Contributopia
## Dégoogliser ne suffit pas

<a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> vous invite à embarquer dans une aventure commune&nbsp;: explorer les mondes numériques où l’humain et ses libertés fondamentales sont respectés, et ce jusque dans la conception des outils.

Dans ce voyage prévu de 2017 à 2020, cherchons ensemble comment concrétiser des outils numériques qui faciliteront les contributions de chacun·e, dans tous nos domaines d’action et de création.

[+ d’infos](https://framablog.org/2017/10/09/contributopia-degoogliser-ne-suffit-pas/)

</p>
{{% grid class="row" %}}
{{% grid class="col-sm-4 text-center" %}}

[![](/menu-services.png)](../services)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### 2017-2018&nbsp;: Créer et proposer des outils

À <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a>, fort·e·s de trois années passées à <a href="https://degooglisons-internet.org"><b class="violet">Dégoogliser</b>&nbsp;<b class="orange">Internet</b></a> en proposant plus d’une trentaine de services alternatifs, on sait que les services en ligne des géants du Web ne font pas que siphonner nos vies numériques&nbsp;: par leur conception même, ils façonnent nos usages, nos actions et notre pensée.

Dans ce premier monde à explorer, les services en ligne sont conçus différemment, et respectent les vies de chacun·e et les diversités qui composent nos réseaux. Un monde où sont conçus des outils numériques facilitant l’exposition commune de nos idées, nos opinions, nos rencontres&hellip; et même de nos vidéos&nbsp;!

[+ d’infos](../services)

{{% /grid %}}
{{% /grid %}}
{{% grid class="row" %}}
{{% grid class="col-sm-4 text-center" %}}

[![](/menu-essaimage.png)](../essaimage)

{{% /grid %}}
{{% grid class="col-sm-8" %}}
    
### 2018-2019&nbsp;: Transmettre les savoir-faire

Les géants du Web se sont créés en centralisant nos données, avec des services séduisants qui enferment nos existences numériques dans de gigantesques silos emplis de nos _data_&nbsp;: des traces de nos clics, de nos échanges et de nos vies. Reproduire ce modèle centralisateur, ce serait concentrer les pouvoirs et les dangers qui vont avec.

Allons découvrir ensemble un monde où l’on ne met pas toutes nos vies dans le même panier numérique&nbsp;! Un monde où, face aux centres commerciaux de nos données, s’organisent des «&nbsp;AMAP&nbsp;» du numérique, qui proposent des services web dans la proximité et l’échange. Un monde où fleurissent <abbr title="Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires">CHATONS</abbr>, nuages personnels, entraides à la formation et pollinisations internationales.


[+ d’infos](../essaimage)

{{% /grid %}}
{{% /grid %}}
{{% grid class="row" %}}
{{% grid class="col-sm-4 text-center" %}}

[![](/menu-educ-pop.png)](../educ-pop)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### 2019-2020&nbsp;: Inspirer les possibles

Il y a une vision derrière les services que nous proposent les géants du Web. Celle qui réduit notre intimité à de la marchandise pour les annonceurs publicitaires. Celle qui nous réduit au rôle de consommatrices et de consommateurs qui abandonnent leurs libertés pour plus de confort. De fait, il n’y a pas de place dans leurs propositions pour des outils numériques facilitant les possibles, la contribution et l’éducation populaire.

Reste un monde encore bien inconnu à co-créer et défricher ensemble, garni d’outils pensés pour développer de nouvelles façons de faire en commun, de faire des communs. Un monde qui s’inspire des pratiques de nombreuses communautés (dont celles du logiciel libre), pour aider chacun·e à choisir les outils qui lui correspondent, atteindre l’autonomie numérique, collaborer différemment et partager les connaissances.

[+ d’infos](../educ-pop)

{{% /grid %}}
{{% /grid %}}
<p>
